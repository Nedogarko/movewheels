<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'communities';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','users','status'];

    public $timestamps = true;

}