<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Charts;

use App\User;

class ChartController extends Controller
{
    public function index()
    {

        $countries = $this->getCountries();
        $male = $this->getMalebyCountries();
        $female = $this->getFemalebyCountries();

        $chart = Charts::multi('line', 'highcharts')
            ->title('Распределение участников по полу в зависимости от страны')
            ->elementLabel('Количество')
            ->labels($countries)
            ->dataset('Male', $male)
            ->dataset('Female', $female)
            ->loader(true)
            ->dimensions(0, 700);

        return view('chart', ['chart' => $chart])->render();
    }

    public function getCountries()
    {
        /**
         * Returns the array with countries
         * @return array
         */

        $contries = User::distinct()->whereNotNull('country_id')->orderBy('country_id', 'asc')->pluck('country_name',
            'country_id');
        $result = [];
        foreach ($contries as $country) {
            $result[] = $country;
        }

        return $result;
    }

    public function getMaleByCountries()
    {
        /**
         * Returns the array with countries and quantities of male users
         * @return array
         */

        $contries = DB::select('
        SELECT c.country_id,c.country_name ,IFNULL(r.kol,0) as quantity
        FROM
        (SELECT DISTINCT country_id,country_name from users where `country_id` is not null) as c
        LEFT JOIN
        (select country_id,country_name, count(id) as kol from `users` where country_id is not null and `sex` = 2 group by country_id,country_name) as r
        ON c.country_id=r.country_id
        ORDER BY c.country_id asc        ');

        $result = [];
        foreach ($contries as $country) {
            $result[] = $country->quantity;
        }

        return $result;
    }

    public function getFemaleByCountries()
    {
        /**
         * Returns the array with countries and quantities of female users
         * @return array
         */

        $contries = DB::select('
            SELECT c.country_id,c.country_name ,IFNULL(r.kol,0) as quantity
            FROM
            (SELECT DISTINCT country_id,country_name from users where `country_id` is not null) as c
            LEFT JOIN
            (select country_id,country_name, count(id) as kol from `users` where country_id is not null and `sex` = 1 group by country_id,country_name) as r
            ON c.country_id=r.country_id
            ORDER BY c.country_id asc
        ');

        $result = [];
        foreach ($contries as $country) {
            $result[] = $country->quantity;
        }

        return $result;
    }
}
