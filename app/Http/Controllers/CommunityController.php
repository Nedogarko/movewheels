<?php

namespace App\Http\Controllers;

use App\Users;
use App\Community;
use Illuminate\Http\Request;

/*
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\DB as DB;
*/

class CommunityController extends Controller
{
    public $uri_users, $uri_regdate, $iteration, $step, $offset, $community, $fields, $version;

    public function __construct()
    {
        $this->uri_users = 'https://api.vk.com/method/groups.getMembers?';
        $this->community = 'group_id=';
        $this->offset = '&offset=';
        $this->fields = '&fields=sex,bdate,city,country';
        $this->version = '&v=5.8';
        $this->iteration = 0;
        $this->step = 1000;
        $this->uri_regdate = 'http://vk.com/foaf.php?id=';
    }

    public function getTotalUsers(Request $request)
    {
        /**
         * Get info about community
         * @param Request $request
         */
        $community = $request->input('public_id');
        $this->community .= $community;
        $offset = $this->offset . 0;
        $request = $this->uri_users . $this->community . $offset . $this->version;

        $data = json_decode(file_get_contents($request));
        if (!isset($data->response)) {
            return response()->json(['error' => 'Public not found']);
        } else {
            $total_users = $data->response->count;
            $this->iteration = ceil($total_users / $this->step);

            $mCommunity = new Community;
            $communityInfo = $mCommunity->where('name', $community)->first();

            return response()->json([
                'name' => $community,
                'total_users' => $total_users,
                'action' => isset($communityInfo) ? 'updateConfirm' : 'createConfirm',
                'invited' => isset($communityInfo) ? $total_users - $communityInfo->users : null
            ]);
        }
    }

    public function parse(Request $request)
    {
        /**
         * Parse community users via API and store in the table
         * @param Request $request
         */
        session_write_close();

        $communityName = $request->input('community_name');
        $communityUsers = $request->input('community_total_users');

        $this->iteration = (int)ceil($communityUsers / $this->step);

        $mCommunity = new Community;

        if (!$community_info = $mCommunity->where('name', $communityName)->first()) {
            $mCommunity->name = $communityName;
            $mCommunity->users = $communityUsers;
            $mCommunity->save();

            $community_info = $mCommunity;
        }

        for ($i = 0; $i < $this->iteration; $i++) {
            $offset = $this->offset . ($i * $this->step);
            $request = $this->uri_users . $this->community . $communityName . $offset . $this->fields . $offset . $this->version;
            $data = json_decode(file_get_contents($request));
            unset($users);

            foreach ($data->response->users as $user) {
                $fullDate = isset($user->bdate) ? explode('.', $user->bdate) : false;

                $users[$user->id] = [
                    "community_id" => $community_info->id,
                    "uid" => $user->id,
                    "first_name" => $user->first_name,
                    "last_name" => $user->last_name,
                    "sex" => $user->sex,
                    "age" => (is_array($fullDate) ? self::getAge($user->bdate) : 0),
                    "birth_day" => (isset($fullDate[0]) ? $fullDate[0] : 0),
                    "birth_month" => (isset($fullDate[1]) ? $fullDate[1] : 0),
                    "birth_year" => (isset($fullDate[2]) ? $fullDate[2] : 0),
                    "banned" => ((isset($user->banned) || isset($user->deactivated)) ? 1 : 0),
                    "country_name" => (isset($user->country)) ? $user->country->title : null,
                    "country_id" => (isset($user->country)) ? $user->country->id : null
                ];
            }
            $this->bulkInsertUsers($users);
        }

        return response()->json(['done' => true], 200);
    }

    public function bulkInsertUsers(array $rows)
    {
        /**
         * Bulk insert of array data
         * @param array $rows , data for insert
         */

        $table = 'users';

        $first = reset($rows);

        $columns = implode(',',
            array_map(function ($value) {
                return "$value";
            }, array_keys($first))
        );

        $values = implode(',', array_map(function ($row) {
                return '(' . implode(',',
                    array_map(function ($value) {
                        return (!is_null($value)) ? ('"' . str_replace('"', '""', $value) . '"') : 'null';
                    }, $row)
                ) . ')';
            }, $rows)
        );

        $updates = implode(',',
            array_map(function ($value) {
                return "$value = VALUES($value)";
            }, array_keys($first))
        );

        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

        return \DB::insert($sql);
    }


    public function getProgress(Request $request)
    {
        /**
         * Returns the current quantity users of group
         * @param Request $request
         * @return string
         */
        session_write_close();

        $mCommunity = new Community;
        if ($cid = $mCommunity->where('name', $request->input('cname'))->first()) {
            return response()->json(Users::where('community_id', $cid->id)->count('id'));
        }
    }

    protected function getAge($birthday)
    {
        /**
         * Returns the current age of user
         * @param string $birthday
         */
        $birthday_timestamp = strtotime($birthday);
        $age = date('Y') - date('Y', $birthday_timestamp);
        if (date('md', $birthday_timestamp) > date('md')) {
            $age--;
        }
        return $age < 0 ? 0 : $age;
    }

    public function clearTables()
    {
        /**
         * Clear tables with data
         */
        Users::query()->truncate();
        Community::query()->truncate();
    }


}
