<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['community','uid','first_name','last_name','sex','age','birth_day','birth_month','birth_year','banned','reg_date','created_at','updated_at'];

}