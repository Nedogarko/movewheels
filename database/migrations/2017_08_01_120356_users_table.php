<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('community_id');
            $table->integer('uid');
            $table->unique(array('community_id', 'uid'));
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('sex')->nullable();;
            $table->integer('age')->nullable();;
            $table->integer('birth_day')->nullable();;
            $table->integer('birth_month')->nullable();;
            $table->integer('birth_year')->nullable();;
            $table->integer('banned');
            $table->integer('country_id')->nullable();;
            $table->string('country_name')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
