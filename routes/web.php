<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CommunityController as Community;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('community',['community'=> false]);
});

Route::post('/', 'CommunityController@getTotalUsers');

Route::post('/getProgress', 'CommunityController@getProgress');

Route::post('/parse', 'CommunityController@parse');

Route::post('/regdate', 'CommunityController@regDate');

Route::post('/clearTables', 'CommunityController@clearTables');

/* Chart routes */

Route::get('/chart', 'ChartController@index');
