@extends('layouts.main')

@section('content')

    {!! $chart->html() !!}

    {!! $chart->script() !!}
@endsection