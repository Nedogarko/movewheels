@extends('layouts.main')

@section('content')
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

    <div class="error">@include('common.errors')</div>

    <div class="check">
        <div>
            <input id="pid" placeholder="Enter public name or ID e.g. cocacola or 19802817" size="75">
            <br>
            <button class="btn-normal _del" id="checkPublic">Check public</button>
            <button class="btn-normal _del" id="clearTables">Clear tables</button>
        </div>
    </div>
    <div id="parse" style="display: none">
        <div class="community_list panel-body">
            <div id="createConfirm" style="display: none">
                <div>Parse users from VK community <b><span id="community_name"></span></b>: have a <b><span id="total_users"></span></b> users total</div>
                <button class="btn-normal _del" id="startParse">Start parse</button>
            </div>

            <div id="updateConfirm" style="display: none">
                <div>This community <b>"<span id="cname"></span>"</b> we already parsed! Try another community, please</div>
                <button class="btn-normal _del" id="startUpdate">Try again</button>
            </div>
        </div>
    </div>
    <div id="parseProgress" style="display: none">
        <div>Collecting user information:</div>
        <div id="progressBar" style="display: none">
            <div>Parsed&nbsp;</div>
            <div id="progress">0%</div>
            <div>&nbsp;of&nbsp;</div>
            <div id="total"></div>
        </div>
        <div id="parseError"></div>
    </div>

    <script>

        $(function(){
            $('#checkPublic').on('click', function(){
                $.ajax({
                    url: '/',
                    method: 'POST',
                    data: {
                        'public_id' :$('#pid').val(),
                        '_token': $('#token').val()
                    }
                }).done(function(e){
                    if(!e.error) {
                        $('#community_name, #cname').text(e.name);
                        $('#total_users').text(e.total_users);

                        if(e.invited != 'null') {
                            $('#invited_users').text(e.invited);
                        }

                        $('#parse, #' + e.action).show();

                        $('.check, .error').hide();
                        $('#parse').show();
                    }else{
                        $('.error').text(e.error).fadeOut().fadeIn();
                    }
                });
            });

            $('#startUpdate').on('click', function(){
                window.location.href = '/';
            });

            $('#startParse').on('click', function(){

                var total = $('#total_users').text(),
                    name  = $('#community_name').text(),
                    token = $('#token').val(),
                    iteration = 1;


                if( !name || total < 1)
                    window.location.href = '/';

                $('#parse').hide();
                $('#parseProgress').show();

                $('#progressBar').css('display','flex');
                $('#total').text(total);

                $.ajax({
                    url: '/parse',
                    method: 'POST',
                    data: {
                        'community_name' : name,
                        'community_total_users': total,
                        '_token': token
                    }
                }).done(function(e){
                    clearInterval(pid);
                    if(e.errorInfo)
                        console.log(e.errorInfo[2]);
                    if(e.done){
                        $('#progressBar').text('Parse Complite!');
                        window.location.href = '/chart';
                    }


                });

                var pid = setInterval(function(e){
                    var prevVal = -1;
                    $.ajax({
                        url: '/getProgress',
                        method: 'post',
                        data:{
                            'cname': name,
                            '_token': token
                        }
                    }).done(function(e){
                        if(prevVal == e) {
                            $('#parseError').text(' Parse Error ');
                            clearInterval(pid);
                        }else {
                            var percent = e / (total / 100);
                            $('#progress').text(parseFloat(percent).toFixed(2) + '%');
                            prevVal = e;
                        }
                    });
                },5000);
            });

            $('#clearTables').on('click', function(){
                $.ajax({
                    url: '/clearTables',
                    method: 'POST',
                    data: {'_token':$('#token').val()}
                }).done(function(){
                    console.log('Tables clear');
                }).fail(function(e){
                    console.log(e);
                });
            });
        });
    </script>

@endsection